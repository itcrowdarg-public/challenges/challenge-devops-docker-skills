FROM itcrowdarg/base-challenge-devops-rails-multibase:ruby_2.7.5

RUN mkdir -p /app
WORKDIR /app

COPY Gemfile Gemfile.lock ./

RUN bundle install --jobs=6 --retry=5

COPY . .

VOLUME /app/public

ARG DB_ENGINE_DEFAULT
ENV DB_ENGINE=$DB_ENGINE_DEFAULT

# DB_ENGINE is mandatory to build this image
# You MUST set the --build-arg DB_ENGINE_DEFAULT=sqlite3
RUN test -n "$DB_ENGINE"

ENTRYPOINT ["/app/docker-entrypoint.sh"]

CMD ["bundle", "exec", "rails s -b 0.0.0.0"]
