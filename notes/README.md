# How to read this file:
- **Title**: hability to evaluate
- **Info**: Information needed by candidate to work
- **Hint**: How we can help, in case info isn't enough
- **Response**: The solution

# Build
Build the rails app docker image
## Info
tag: `itcrowdarg/challenge-devops-rails-multibase`

```
docker build -t itcrowdarg/challenge-devops-rails-multibase:latest .
```
## Hint
Check for `RUN test -n "$DB_ENGINE"` line in Dockerfile and read comments.

Paramenter: `--build-arg DB_ENGINE_DEFAULT=sqlite3`

## Response
```
docker build --build-arg DB_ENGINE_DEFAULT=sqlite3 -t itcrowdarg/challenge-devops-rails-multibase:latest .
```

# Run in development mode
Run the docker image in `development` mode.

The docker is ready, it needs only `docker run...`
## Info
Rails app use port `3000`, and `develpment` mode is by default.

## Response
```
docker run -p 3000:3000 itcrowdarg/challenge-devops-rails-multibase:latest
```
And browse: `http://localhost:3000/`

# Run in production mode
Run the docker image in `production` mode.

## Info
Environment variable `RAILS_ENV=production` on docker run command
## Hint
Add the new variable with `-e` to command line.

## Response
```
docker run -p 3000:3000 -e RAILS_ENV=production itcrowdarg/challenge-devops-rails-multibase:latest
```
And browse: `http://localhost:3000/`: an **ERROR** appears.

# Debug error

## Info
Rails print log to standard output if environment variable `RAILS_LOG_TO_STDOUT=1` is set

## Hint
Add the new variable with `-e` to command line.

## Response
```
docker run -p 3000:3000 -e RAILS_ENV=production -e RAILS_LOG_TO_STDOUT=1 itcrowdarg/challenge-devops-rails-multibase:latest
```
Once the log is shown, move to next step: **Assets error**

# Assets error
The previous step shows the error:
```
The asset "rails.png" is not present in the asset pipeline
```

## Info
This is because Rails needs the assets precompiled when running in `production` mode.

In a different console, without stopping current docker, run a bash command and then execute:
```
DB_ENGINE=sqlite3 rails assets:precompile
rails restart
```

# Hint
Use `docker exec` to run a bash in the same docker is already running.

## Response
```
docker ps # get [DOCKER ID]
docker exec -it [DOCKER ID] bash
DB_ENGINE=sqlite3 rails assets:precompile
rails restart
```
And then browse to `http://localhost:3000/` again: success!

# Add assets:precompile to Dockerfile, build and run
Edit Dockerfile to add the `assets:precompile` command.

The precompile command can be only executed after all files were copied into the docker

## Hint
Add the command after `COPY . .` line

## Response
`Dockerfile`
```
COPY . .
RUN DB_ENGINE=sqlite3 rails assets:precompile
```
`console`
```
# then re-build:
docker build --build-arg DB_ENGINE_DEFAULT=sqlite3 -t itcrowdarg/challenge-devops-rails-multibase:latest .
# then run:
docker run -p 3000:3000 -e RAILS_ENV=production -e RAILS_LOG_TO_STDOUT=1 itcrowdarg/challenge-devops-rails-multibase:latest
```

And then browse to `http://localhost:3000/` again: success!

# (Optional - Easy) add a new env var: RAILS_SERVE_STATIC_FILES
Add a new env var: `RAILS_SERVE_STATIC_FILES=1`
## Hint
Add it to the command line with `-e`
## Response
```
docker run -p 3000:3000 -e RAILS_ENV=production -e RAILS_LOG_TO_STDOUT=1 -e RAILS_SERVE_STATIC_FILES=1 itcrowdarg/challenge-devops-rails-multibase:latest
```
And then browse to `http://localhost:3000/` again: New image shown!
# Docker compose
Look for the `docker-compose.yml` file.

Complete docker image name, and other necessary things to run the docker image with docker-compose

## Hint
Same docker image: `itcrowdarg/challenge-devops-rails-multibase:latest`

Port: `3000:3000`

## Response
```
version: "3"
services:
  web:
    image: itcrowdarg/challenge-devops-rails-multibase:latest
    ports:
      - "3000:3000"
    environment:
      - RAILS_ENV=production
      - RAILS_LOG_TO_STDOUT=1
      - RAILS_SERVE_STATIC_FILES=1
      - REDIS_URL=XXX
```
and run:
```
docker-compose up
```
# Redis
Add service redis to docker-compose
## Info
Click on `Sidekiq dashboard` => Error

The app needs `redis` server.

Add it to docker-compose

## Hint
```
  redis-server:
    image: redis
```
and
```
- REDIS_URL=XXX # it is ONLY the URL (or IP) not the protocol or port.
```
## Response
```
version: "3"
services:
  web:
    image: itcrowdarg/challenge-devops-rails-multibase:latest
    ports:
      - "3000:3000"
    environment:
      - RAILS_ENV=production
      - RAILS_LOG_TO_STDOUT=1
      - RAILS_SERVE_STATIC_FILES=1
      - REDIS_URL=redis-server
  redis-server:
    image: redis
```
and run:
```
docker-compose up
```