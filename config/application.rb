require_relative "boot"

require "rails/all"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module RailsMultibase
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.1

    case ENV['DB_ENGINE']
    when 'postgresql'
      ENV['DATABASE_DOCKER_HOST'] = 'postgresql'
    when 'mysql'
      ENV['DATABASE_DOCKER_HOST'] = 'mysql'
    when 'sqlite3'
      ENV['DATABASE_DOCKER_HOST'] = 'sqlite3'
    else
      raise ' ======> DB_ENGINE env var MUST be set to [ postgresql | mysql | sqlite3 ]'
    end

    self.paths['config/database'] = "config/database-#{ENV['DB_ENGINE']}.yml"

    # Configuration for the application, engines, and railties goes here.
    #
    # These settings can be overridden in specific environments using the files
    # in config/environments, which are processed later.
    #
    # config.time_zone = "Central Time (US & Canada)"
    # config.eager_load_paths << Rails.root.join("extras")
  end
end
