require 'sidekiq/web'


Rails.application.routes.draw do
  Healthcheck.routes(self)
  get '/' => 'welcome#index', as: :root
  get '/new-sidekiq-job' => 'welcome#sidekiq', as: :new_sidekiq_job
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  mount Sidekiq::Web => '/sidekiq'

end
