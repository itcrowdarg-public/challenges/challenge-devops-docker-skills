# Docker image base for Devops-challenge

Image created and stored at `itcrowdarg/base-challenge-devops-rails-multibase:ruby_2.7.5`

If a change needs to be done, re-publish it with:

```
docker build -f ./docker/Dockerfile -t itcrowdarg/base-challenge-devops-rails-multibase:ruby_2.7.5 .
docker push itcrowdarg/base-challenge-devops-rails-multibase:ruby_2.7.5
```