# Rails sample app Multibase

Sample app to test AWS ECS infrastructure.

## Env vars:

- DB_ENGINE = [postgresql, mysql, sqlite3] *required
- DATABASE_HOST (not necessary if you select `sqlite3`)
- DATABASE_USER (not necessary if you select `sqlite3`)
- DATABASE_PASS (not necessary if you select `sqlite3`)


## Build
To build this image the `DB_ENGINE_DEFAULT` build arg parameter needs to be used:
```
docker build --build-arg DB_ENGINE_DEFAULT=mysql -t itcrowdarg/rails-multibase:mysql .
docker push itcrowdarg/rails-multibase:mysql

docker build --build-arg DB_ENGINE_DEFAULT=postgresql -t itcrowdarg/rails-multibase:postgresql .
docker push itcrowdarg/rails-multibase:postgresql

docker build --build-arg DB_ENGINE_DEFAULT=sqlite3 -t itcrowdarg/rails-multibase:sqlite3 .
docker push itcrowdarg/rails-multibase:sqlite3
```
## Run
From command line:
```
DB_ENGINE=[postgresql, mysql, sqlite3] RAILS_ENV=production rails s -b 0.0.0.0
```

From docker selecting engine from env var:
```
docker run -it --rm -e DB_ENGINE=[postgresql, mysql, sqlite3] -e RAILS_ENV=production itcrowdarg/rails-multibase:latest
```

From docker selecting enging from docker tag
```
docker run -it --rm -e RAILS_ENV=production itcrowdarg/rails-multibase:mysql
```