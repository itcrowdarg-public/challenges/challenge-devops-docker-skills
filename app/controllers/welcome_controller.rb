class WelcomeController < ApplicationController
  def index
    @config = Rails.configuration.database_configuration
    @db_connected = db_connected?
  end

  def sidekiq
    MyWorker.perform_async(DateTime.now.to_s)
    redirect_to :root
  end

  private

  def db_connected?
    ActiveRecord::Base.establish_connection # Establishes connection
    ActiveRecord::Base.connection # Calls connection object
    ActiveRecord::Base.connected? 
    ActiveRecord::Base.connected?
  rescue
    false
  end
end
