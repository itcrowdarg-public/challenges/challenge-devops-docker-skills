class MyWorker
  include Sidekiq::Worker
  def perform(date_time)
    puts "Working within batch #{date_time}"
  end
end